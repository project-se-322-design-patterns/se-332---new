package app;

public class HundredThousandExpression extends Expression {

    @Override
    public String One() {
        return "G";
    }
          //G = 100,000 
    @Override
    public String Four() {
        return "GH";
    }
   //H= 500,000
    @Override
    public String Five() {
        return "H";
    }
 //J = 1,000,000 
    @Override
    public String Nine() {
        return "GJ";
    }

    @Override
    public int Multiplier() {
        return 100000;
    }

}