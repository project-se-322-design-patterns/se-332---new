package app;

public class TenThousandExpression extends Expression {

    @Override
    public String One() {
        return "S";
    }
          //F = 50,000 
    @Override
    public String Four() {
        return "SF";
    }

    @Override
    public String Five() {
        return "F";
    }
 //G = 100,000 
    @Override
    public String Nine() {
        return "SG";
    }

    @Override
    public int Multiplier() {
        return 10000;
    }

}