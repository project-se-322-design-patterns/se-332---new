package app;

public class MillionExpression extends Expression {

    @Override
    public String One() {
        return "J";
    }
          //K = 5,000,000 
    @Override
    public String Four() {
        return "JK";
    }

    @Override
    public String Five() {
        return "K";
    }
 //B = 10,000,000 
    @Override
    public String Nine() {
        return "JB";
    }

    @Override
    public int Multiplier() {
        return 1000000;
    }

}