package app;

public class Context{
    private String sinput;
    private int ioutput;

    public Context(String in){
        this.sinput = in;
    }

    public String getSinput() {
        return sinput;
    }

    public void setSinput(String sinput) {
        this.sinput = sinput;
    }

    public int getIoutput() {
        return ioutput;
    }

    public void setIoutput(int ioutput) {
        this.ioutput = ioutput;
    }
}