package app;

public abstract class Expression{
    public void Interpret(Context context){
        if(context.getSinput().length() == 0){
            return;
        }

        if(context.getSinput().startsWith(Nine())){
            int s = context.getIoutput();
            s += (9 * Multiplier());
            context.setIoutput(s);
            String n = context.getSinput();
            context.setSinput(n.substring(2));
        }
        else if(context.getSinput().startsWith(Four())){
            int s = context.getIoutput();
            s += (4 * Multiplier());
            context.setIoutput(s);
            String n = context.getSinput();
            context.setSinput(n.substring(2));
        }
        else if(context.getSinput().startsWith(Five())){
            int s = context.getIoutput();
            s += (5 * Multiplier());
            context.setIoutput(s);
            String n = context.getSinput();
            context.setSinput(n.substring(1));
        }
        else if(context.getSinput().startsWith(One())){
            int s = context.getIoutput();
            s += (1 * Multiplier());
            context.setIoutput(s);
            String n = context.getSinput();
            context.setSinput(n.substring(1));
        }

    }

    public abstract String One();
    public abstract String Four();
    public abstract String Five();
    public abstract String Nine();
    public abstract int Multiplier();
}