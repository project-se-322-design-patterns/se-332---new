package app;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Interpreter");
        String roman = "JBGJSGMSCMXCIX";
        Context context  = new Context(roman);
        // **NOTE**
        //  X = 10
        //  C = 100
        //  D = 500
        //  M = 1,000
        //  A = 5,000
        //  S = 10,000
        //  F = 50,000
        //  G = 100,000
        //  H = 500,000
        //  J = 1,000,000
        //  K = 5,000,000
        //  B = 10,000,000
        //  Z = 50,000,000
        List<Expression> tree = new ArrayList<Expression>();
        tree.add(new TenMillionExpression()); 
        tree.add(new MillionExpression()); 
        tree.add(new HundredThousandExpression()); 
        tree.add(new TenThousandExpression()); 
        tree.add(new ThousandExpression());      
        tree.add(new HundredExpression());
        tree.add(new TenExpression());
        tree.add(new OneExpression());

        for(int i = 0;i<roman.length();i++){
            for(Expression e : tree){
                e.Interpret(context);
            }
        }
        System.out.println(roman + " = " + context.getIoutput());
    }
}