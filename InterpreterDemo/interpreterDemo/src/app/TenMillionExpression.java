package app;

public class TenMillionExpression extends Expression {

    @Override
    public String One() {
        return "B";
    }
          //Z = 50,000,000 
    @Override
    public String Four() {
        return "BZ";
    }

    @Override
    public String Five() {
        return "Z";
    }
 //B = 100,000,000 
    @Override
    public String Nine() {
        return "BN";
    }

    @Override
    public int Multiplier() {
        return 10000000;
    }

}