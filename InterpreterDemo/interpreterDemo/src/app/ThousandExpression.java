package app;

public class ThousandExpression extends Expression{

    @Override
    public String One() {
        return "M";
    }

    @Override
    public String Four() {
        return "MA";
    }
//A = 5000
    @Override
    public String Five() {
        return "A";
    }
//S = 10000
    @Override
    public String Nine() {
        return "MS";
    }

    @Override
    public int Multiplier() {
        return 1000;
    }

}