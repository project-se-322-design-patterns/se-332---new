package app;

import java.text.NumberFormat;

import javax.swing.JFrame;

import app.forest.ForestFly;
import app.forest.ForestNormal;
import app.forest.*;
import app.trees.*;
import java.awt.*;

// public class App {
//     public static void main(String[] args) throws Exception {
//         System.out.println("Flyweight Demo");
       
//         // long startTime = System.currentTimeMillis();
//         for(int i=0; i < 20; ++i) {
//             Circle circle = (Circle)FlyweightFactory.getCircle(getRandomColor());
//             circle.setX(20);
//             circle.setY(20);
//             circle.setRadius(100);
//             circle.draw();}
      
// long total = Runtime.getRuntime().totalMemory();
// long used  = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
// System.out.println("use Memmory:" +(used));

//     }
//     private static Circle Circle(String randomColor) {
// 		return null;
// 	}
// 	private static final String colors[] = { "Red" };
    
//     private static String getRandomColor() {
//         return colors[(int)(Math.random()*colors.length)];
//      }
    
//      private static int getRandomX() {
//         return (int)(Math.random()*100 );
//     }
//     private static int getRandomY() {
//        return (int)(Math.random()*100);
//     }
    
// }
public class App {
    static int CANVAS_SIZE = 500;
    static int TREES_TO_DRAW = 100000;
    static int TREE_TYPES = 2;

    public static void main(String[] args) {
        int mb = 1024*1024;

        //Normal
        long beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();

        ForestNormal forest = new ForestNormal();
        for (int i = 0; i < Math.floor(TREES_TO_DRAW / TREE_TYPES); i++) {
            forest.plantTree(random(0, CANVAS_SIZE), random(0, CANVAS_SIZE),
                    "Summer Oak", Color.GREEN, "Oak texture stub");
            forest.plantTree(random(0, CANVAS_SIZE), random(0, CANVAS_SIZE),
                    "Autumn Oak", Color.ORANGE, "Autumn Oak texture stub");
        }
        forest.setTitle("Use Flyweight");
        forest.setSize(CANVAS_SIZE, CANVAS_SIZE);
        forest.setVisible(true);

        long afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();

        long actualMemUsed=afterUsedMem-beforeUsedMem;

        //Use Flyweight
        long beforeUsedMemF=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();

        ForestFly forest1 = new ForestFly();
        for (int i = 0; i < Math.floor(TREES_TO_DRAW / TREE_TYPES); i++) {
            forest1.plantTree(random(0, CANVAS_SIZE), random(0, CANVAS_SIZE),
                    "Summer Oak", Color.GREEN, "Oak texture stub");
            forest1.plantTree(random(0, CANVAS_SIZE), random(0, CANVAS_SIZE),
                    "Autumn Oak", Color.ORANGE, "Autumn Oak texture stub");
        }
        forest1.setTitle("Not use Flyweight");
        forest1.setSize(CANVAS_SIZE, CANVAS_SIZE);
        forest1.setVisible(true);

        long afterUsedMemF=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();

        long actualMemUsedF=afterUsedMemF-beforeUsedMemF;

        System.out.println("Normal Used Memory: "+(actualMemUsed / mb)+" MB");
        System.out.println("Flyweight Used Memory: "+(actualMemUsedF / mb)+" MB");


    }

    private static int random(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }
}